package com.myvicino.catalog.domain;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

/**
 * This entity will work like stock keeping units.
 * 
 * @author pradeep.kaushal
 *
 */
@Entity
public class SKU extends BaseEntity{
	@Id
	@GeneratedValue
	private Long id;
	
	/**
	 * Vendors who are selling this product
	 */
	@ManyToOne(targetEntity = Vendor.class, fetch = FetchType.EAGER)
	private List<Vendor> vendors;
	
	/**
	 * Products who are selling this product.
	 */
	@ManyToOne(targetEntity = Product.class, fetch = FetchType.EAGER)
	private List<Product> products;
	/**
	 * One stock unit will have one price for the one product
	 */
	@OneToOne(targetEntity=Price.class)
	private Price price;
	/**
	 * The quantity vendor has
	 */
	@Column
	private Long quantity;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Vendor> getVendors() {
		return vendors;
	}

	public void setVendors(List<Vendor> vendors) {
		this.vendors = vendors;
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}

	public Long getQuantity() {
		return quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

}
