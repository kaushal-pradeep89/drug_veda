package com.myvicino.catalog.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * This is the price of the product. It will be totally independent of the
 * vendor and product.
 * 
 * @author pradeep.kaushal
 *
 */
@Entity
public class Price extends BaseEntity{
	@Id
	@GeneratedValue
	private Long id;

	/**
	 * The actual price
	 */
	@Column
	private Double price;
	/**
	 * The bulck price
	 */
	@Column
	private Double bulkPrice;
	/**
	 * offer price
	 */
	@Column
	private Double offerPrice;
	/**
	 * The percent of discount
	 */
	@Column
	private Double discount;
	/**
	 * Createion date
	 */
	@Column
	private Date createDate;
	/**
	 * private
	 */
	@Column
	private Date updateDate;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Double getBulkPrice() {
		return bulkPrice;
	}
	public void setBulkPrice(Double bulkPrice) {
		this.bulkPrice = bulkPrice;
	}
	public Double getOfferPrice() {
		return offerPrice;
	}
	public void setOfferPrice(Double offerPrice) {
		this.offerPrice = offerPrice;
	}
	public Double getDiscount() {
		return discount;
	}
	public void setDiscount(Double discount) {
		this.discount = discount;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

}
