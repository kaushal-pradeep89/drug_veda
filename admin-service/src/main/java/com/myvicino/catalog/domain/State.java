package com.myvicino.catalog.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
/**
 * This entity will only hold the state information of the vendor.
 * This will be master value for the tables.
 * @author pradeep.kaushal
 *
 */
@Entity
public class State extends BaseEntity{
	@Id
	@GeneratedValue
	private Long id;
	
	@Column
	private String stateName;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getStateName() {
		return stateName;
	}
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	
	
}
