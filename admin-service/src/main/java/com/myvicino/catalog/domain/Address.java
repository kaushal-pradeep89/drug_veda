package com.myvicino.catalog.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * Created by pradeep.kaushal on 08/08/15.
 */
@Entity
//@Table(name = "address_tbl")
public class Address extends BaseEntity {
    @Id
    @GeneratedValue
    private Long id;
    @Column
    private String addressLine;
    @Column(length=6)
    private String zipCode;
    @Column
    private String city;
    
    @ManyToOne(targetEntity=State.class)
    private State state;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAddressLine() {
		return addressLine;
	}

	public void setAddressLine(String addressLine) {
		this.addressLine = addressLine;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

    
   
}
