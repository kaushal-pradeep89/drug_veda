package com.myvicino.catalog.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by pradeep.kaushal on 08/08/15.
 */
@Entity
// @Table(name="product_type_tbl")
public class ProductType extends BaseEntity {
	@Id
	@GeneratedValue
	private Long id;
	/**
	 * This defines whether product will be capasules,tablet or any medical
	 * device.
	 */
	@Column
	private String productType;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}
	
}
