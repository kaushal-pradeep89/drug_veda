package com.myvicino.catalog.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by pradeep.kaushal on 08/08/15.
 */
@Entity
public class ProductImage extends BaseEntity {
	@Id
	@GeneratedValue
	private Long id;
	@Column
	private String imageName;
	@Column
	private String imgUrl;
	@Column
	private Boolean def;
	@Column
	private Boolean delete;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getImageName() {
		return imageName;
	}
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
	public String getImgUrl() {
		return imgUrl;
	}
	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}
	public Boolean getDef() {
		return def;
	}
	public void setDef(Boolean def) {
		this.def = def;
	}
	public Boolean getDelete() {
		return delete;
	}
	public void setDelete(Boolean delete) {
		this.delete = delete;
	}
	
}
