package com.myvicino.catalog;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ImportResource("classpath:admin-service.xml")
public class AdminServiceConfig {

}
